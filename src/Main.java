import java.util.*;
import java.util.stream.Stream;

public class Main {
    //        같은 숫자는 싫어
//    public static void main(String[] args) {
//        int[] testData = {1,1,3,3,0,1,1};
//        System.out.println(solution(testData));
//    }
//    public static Stack<Integer> solution(int[] arr) {
//        Stack<Integer> answer = new Stack<>();
//        for(int i = 0; i < arr.length; i++) if(answer.isEmpty() || arr[i] != answer.peek()) answer.push(arr[i]);
//        return answer;
//    }

//    올바른 괄호
//public static void main(String[] args) {
//        String test = "())(()";
//        System.out.println(solution(test));
//}
//    static boolean solution(String s) {
//        boolean answer = true;
//
//        Stack<String> stack = new Stack<>();
//
//        for (int i = 0; i < s.length(); i++) {
//            if (s.charAt(0) == ')') {
//                answer = false;
//                break;
//            } else if ('(' == s.charAt(i)) {
//                stack.push("(");
//            } else if (!stack.isEmpty()) {
//                stack.pop();
//            } else answer = false;
//        }
//
//        if (!stack.isEmpty()) answer = false;
//
//        return answer;
//    }

    // 부족한 금액 계산하기
//    public static void main(String[] args) {
//        int price = 3;
//        int money = 20;
//        int count = 4;
//        System.out.println(solution(price, money, count));
//    }
//
//    public static long solution(long price, long money, long count) {
//        return Math.max(price * (count * (count + 1) / 2) - money, 0);
//    }

    // 완주 하지 못한 선수 (해시맵)
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Objects;
//
//public class Main {
//    public static void main(String[] args) {
//        String[] participant = {"mislav", "stanko", "mislav", "ana"};
//        String[] completion = {"stanko", "ana", "mislav"};
//
//        System.out.println(solution(participant, completion));
//    }
//
//    public static String solution(String[] participant, String[] completion) {
//        HashMap<String, Integer> map = new HashMap<>();
//
//        for (String s : participant) {
//            if (map.containsKey(s)) map.put(s, map.get(s) + 1);
//            else map.put(s, 1);
//        }
//
////        for (String s : participant) map.put(s, map.getOrDefault(s, 0) + 1);
//        for (String s : completion) map.put(s, map.get(s) - 1);
//
//        return map.entrySet().stream().filter(entry -> Objects.equals(entry.getValue(), 1)).map(Map.Entry::getKey).findFirst().orElse("");
////        return map.entrySet().stream().filter(entry -> entry.getValue() != 0).map(Map.Entry::getKey).findFirst().orElse("");
//    }
//}

//import java.util.HashMap;
//
//    // 폰켓몬 (해시맵)
//    public class Main {
//        public static void main(String[] args) {
//            int[] nums = {3,3,3,2,2,2};
//
//            System.out.println(solution(nums));
//        }
//
//        public static int solution(int[] nums) {
//            HashMap<Integer, Integer> map = new HashMap<>();
//
//            for (int p : nums) map.put(p, 1);
//
//            return Math.min(nums.length / 2, map.size());
//        }
//    }

    // 하노이의 탑
//    public static void main(String[] args) {
//        int n = 2;
//        System.out.println(Arrays.deepToString(solution(n)));
//    }
//
//    public static int[][] solution(int n) {
//        List<int[]> moves = new ArrayList<>();
//        hanoi(n, 1, 3, 2, moves);
//        return moves.toArray(new int[0][]);
//    }
//
//    private static void hanoi(int n, int from, int to, int mid, List<int[]> moves) {
//        if (n == 0) return;
//        hanoi(n - 1, from, mid, to, moves);
//        moves.add(new int[]{from, to});
//        hanoi(n - 1, mid, to, from, moves);
//    }

    // 두 정수 사이의 합
//    public static void main(String[] args) {
//        int a = 3;
//        int b = 3;
//        System.out.println(solution(a, b));
//    }
//
//    public static long solution(int a, int b) {
//        return (long) (Math.abs(a - b) + 1) * (a + b) / 2 ;
//    }

    // 문자열 내 p와 y의 개수
//    public static void main(String[] args) {
//        String s = "Pyy";
//        System.out.println(solution(s));
//    }
//
//    static boolean solution(String s) {
//        char[] chars = s.toLowerCase().toCharArray();
//        int count = 0;
//        for (char c : chars) {
//            if (c == 'p') count++;
//            else if (c == 'y') count--;
//        }
//        return count == 0 ? true : false;
//    }
//    // return s.chars().filter( e -> 'P'== e).count() == s.chars().filter( e -> 'Y'== e).count();

    // 문자열을 정수로 바꾸기
//    public static void main(String[] args) {
//        String temp = "-1234";
//        System.out.println(solution(temp));
//    }
//
//    public static int solution(String s) {
//        return s.charAt(0) == '-' ? Integer.parseInt(s.substring(1, s.length())) * -1 : Integer.parseInt(s);
//    }

    // 자릿수 더하기
//    public static void main(String[] args) {
//        int n = 123;
//        System.out.println(solution(n));
//    }
//
//    public static int solution(int n) {
//        int answer = 0;
//        char[] arr = Integer.toString(n).toCharArray();
//        for (char c : arr) answer += Integer.parseInt(String.valueOf(c));
//
//        return answer;
//    }

    // 자연수 뒤집어 배열로 만들기
//    public static void main(String[] args) {
//        System.out.println(Arrays.toString(solution(10000000000L)));
//    }
//
//    public static int[] solution(long n) {
//        char[] arr = Long.toString(n).toCharArray();
//        int[] answer = new int[arr.length];
//
//        for (int i = 0; i < Math.floor((double) (arr.length - 1) / 2); i++) {
//            char temp = arr[i];
//            arr[i] = arr[arr.length - 1 - i];
//            arr[arr.length - 1 - i] = temp;
//        };
//
//        for (int i = 0; i < arr.length; i++) answer[i] = Character.getNumericValue(arr[i]);
//
//        return answer;
//    }

    // 정수 내림차순으로 배치하기
//    public static void main(String[] args) {
//        long n = 118372;
//        System.out.println(solution(n));
//    }
//
//    public static long solution(long n) {
//        char[] charArr = Long.toString(n).toCharArray();
//        Character[] arr = new Character[charArr.length];
//        Arrays.sort(arr, Character::compareTo);
//
//        return arr;
//    }

    // 안전지대
//    public static void main(String[] args) {
//        int[][] board = {{1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1}};
//        System.out.println(solution(board));
//    }
//
//    public static int solution(int[][] board) {
//        int answer = 0;
//
//        int[][] coordinates = {{-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}};
//
//        for (int i = 0; i < board.length; i++) {
//            for (int j = 0; j < board[0].length; j++) {
//                if(board[i][j] == 1) {
//                    for (int k = 0; k < coordinates.length; k++) {
//                        try {
//                            if (board[i + coordinates[k][0]][j + coordinates[k][1]] == 0) board[i + coordinates[k][0]][j + coordinates[k][1]] = 2;
//                        } catch (ArrayIndexOutOfBoundsException e) {
//                            continue;
//                        }
//                    }
//                }
//            }
//        }
//
//        return Arrays.stream(board).flatMapToInt(Arrays::stream).filter(v -> v == 0).toArray().length;
//    }

    // 크기가 작은 부분 문자열
//    public static void main(String[] args) {
//        System.out.println(solution("3141592", "271"));
//    }
//
//    public static int solution(String t, String p) {
//        int answer = 0;
//
//        for(int i = 0; i < t.length() - p.length() + 1; i++) {
//            long temp = Long.parseLong(t.substring(i, i + p.length()));
//            if(temp <= Long.parseLong(p)) answer++;
//        }
//
//        return answer;
//    }

    //  이상한 문자 만들기
//    public static void main(String[] args) {
//        System.out.println(solution("tryi hello world"));
//    }
//
//    public static String solution(String s) {
//        StringBuilder answer = new StringBuilder();
//        String[] words = s.split(" ", -1); // 공백을 포함한 모든 단어를 구분
//        for (String word : words) {
//            for (int i = 0; i < word.length(); i++) {
//                if (i % 2 == 0) {
//                    answer.append(Character.toUpperCase(word.charAt(i)));
//                } else {
//                    answer.append(Character.toLowerCase(word.charAt(i)));
//                }
//            }
//            answer.append(" "); // 단어 사이에 공백 추가
//        }
//        return s.charAt(s.length() - 1) == ' ' ? answer.toString() : answer.toString().trim(); // 마지막 공백 제거
//    }

    // 이상한 문자 만들기 보류
//    public static void main(String[] args) {
//        System.out.println(solution(" jump  space one two  three   "));
//    }
//
//    public static String solution(String s) {
//            StringBuilder answer = new StringBuilder();
//            String[] words = s.split(" ", -1); // 공백을 포함한 모든 단어를 구분
//
//            for (String word : words) {
//                for (int i = 0; i < word.length(); i++) {
//                    if (i % 2 == 0) {
//                        answer.append(Character.toUpperCase(word.charAt(i)));
//                    } else {
//                        answer.append(Character.toLowerCase(word.charAt(i)));
//                    }
//                }
//                answer.append(" "); // 단어 사이에 공백 추가
//            }
//
//            return s.charAt(s.length() - 1) == ' ' ? answer.substring(0, answer.length() - 1) : answer.toString();
//    }기

    // 삼총사
//    public static void main(String[] args) {
//        System.out.println(solution(new int[]{-3, -2, -1, 0, 1, 2, 3}));
//    }
//
//    public static int solution(int[] number) {
//        int answer = 0;
//
//        for (int i = 0; i < number.length; i++) {
//            for (int j = i + 1; j < number.length; j++) {
//                for (int k = j + 1; k < number.length; k++) {
//                    if(number[i] + number[j] + number[k] == 0) answer++;
//                }
//            }
//        }
//
//        return answer;
//    }

    // 최소직사각형
//    public static void main(String[] args) {
//        System.out.println(solution(new int[][]{{60, 50}, {30, 70}, {60, 30}, {80, 40}}));
//    }
//
//    public static int solution(int[][] sizes) {
//        // 제일 큰 수 찾음
//        int a = Arrays.stream(sizes).parallel().flatMapToInt(Arrays::stream).summaryStatistics().getMax();
//        // 각 배열들에서 작은 값 중에 제일 큰거
//        int b = 0;
//        for (int[] size : sizes) {
//            int temp = size[0];
//            if (temp > size[1]) temp = size[1];
//            if (temp > b) b = temp;
//        }
//
//        return a * b;
//    }

    // 시저 암호
//    public static void main(String[] args) {
//        System.out.println(solution("a B z", 25));
//    }
//
//    public static StringBuilder solution(String s, int n) {
//        StringBuilder answer = new StringBuilder(s);
//        char[] chars = s.toCharArray();
//
//        for (int i = 0; i < s.length(); i++) {
//            if (chars[i] == ' ') answer.setCharAt(i, ' ');
//            else if (chars[i] < 91 && chars[i] + n > 90) answer.setCharAt(i, (char) (chars[i] + n - 26));
//            else if (chars[i] > 96 && chars[i] + n > 122) answer.setCharAt(i, (char) (chars[i] + n - 26));
//            else answer.setCharAt(i, (char) (chars[i] + n));
//        }
//
//        return answer;
//    }

    // 가장 가까운 같은 글자
//    public static void main(String[] args) {
//        System.out.println(Arrays.toString(solution("banana")));
//    }
//
//    public static int[] solution(String s) {
//        List<String> arr = new ArrayList<>();
//        int[] answer = new int[s.length()];  // 배열 크기 수정
//
//        for (int i = 0; i < s.length(); i++) {
//            if (!arr.contains(String.valueOf(s.charAt(i)))) {
//                answer[i] = -1;  // 조건에 맞는 경우 -1
//                arr.add(String.valueOf(s.charAt(i)));
//            } else {
//                answer[i] = i - arr.lastIndexOf(String.valueOf(s.charAt(i)));
//                arr.add(String.valueOf(s.charAt(i)));
//            }
//        }
//
//        return answer;
//    }

//    K번째수
//    public static void main(String[] args) {
//        int[] array = {1, 5, 2, 6, 3, 7, 4};
//        int[][] commands = {{2, 5, 3}, {4, 4, 1}, {1, 7, 3}};
//        System.out.println(Arrays.toString(solution(array, commands)));
//    }
//
//    public static int[] solution(int[] array, int[][] commands) {
//        List<Integer> answer = new ArrayList<>();
//
//        for (int i = 0; i < commands.length; i++) {
//            List<Integer> temp = new ArrayList<>();
//            for (int j = commands[i][0] - 1; j < commands[i][1]; j++) temp.add(array[j]);
//            temp.sort(Comparator.naturalOrder());
//            answer.add(temp.get(commands[i][2] - 1));
//        }
//
//        return answer.stream().mapToInt(Integer::intValue).toArray();
//    }

//    두 개 뽑아서 더하기
//    public static void main(String[] args) {
//        System.out.println(solution(new int[] {2,1,3,4,1}));
//    }
//
//    public static List<Integer> solution(int[] numbers) {
//        List<Integer> answer = new ArrayList<>();
//
//
//        for (int i = 0; i < numbers.length; i++) {
//            for (int j = 0; j < numbers.length; j++) {
//                if (i != j && !answer.contains(numbers[i] + numbers[j])) answer.add(numbers[i] + numbers[j]);
//            }
//        }
//        answer.sort(Comparator.naturalOrder());
//        return answer;
//    }

//    [PCCE 기출문제] 9번 / 지폐 접기
//    public static void main(String[] args) {
//        System.out.println(solution(new int[]{30, 15}, new int[]{26, 17}));
//    }

//     public int solution(int[] wallet, int[] bill) {
//         int answer = 0;
        
//         while (Math.min(bill[0], bill[1]) > Math.min(wallet[0], wallet[1]) || Math.max(bill[0], bill[1]) > Math.max(wallet[0], wallet[1])) {
//             if (bill[0] > bill[1]) bill[0] /= 2;
//             else bill[1] /= 2;
//             answer++;
//         }
        
//         return answer;
//     }

//  로또의 최고 순위와 최저 순위
    // public static void main(String[] args) {
    //     System.out.println(Arrays.toString(solution(new int[]{1, 2, 3, 4, 5, 6}, new int[]{7, 8, 9, 10, 11, 12})));
    // }

    // public static int[] solution(int[] lottos, int[] win_nums) {
    //     int[] answer = new int[2];
    //     int count = 0;
    //     int zeroCnt = 0;

    //     for (int lotto : lottos) {
    //         if (lotto == 0) zeroCnt++;
    //         for (int winNum : win_nums) {
    //             if (lotto == winNum) count++;
    //         }
    //     }

    //     answer[0] = 7 - count - zeroCnt == 7 ? 6 : 7 - count - zeroCnt;
    //     answer[1] = 7 - count == 7 ? 6 : 7 - count;

    //     return answer;
    // }

//  과일 장수
    // public static void main(String[] args) {
    //     System.out.println(solution(3, 4, new int[]{1, 2, 3, 1, 2, 3, 1}));
    // }

    // public static int solution(int k, int m, int[] score) {
    //     int answer = 0;
        
    //     Arrays.sort(score);
        
    //     for (int i = score.length - m; i >= 0 / m; i-=m) answer += score[i] * m;
        
    //     return answer;
    // }

// 성격 유형 검사하기
    // public static void main(String[] args) {
    //     System.out.println(solution(new String[]{"AN", "CF", "MJ", "RT", "NA"}, new int[]{5, 3, 2, 7, 5}));
    // }
// 
    // public static StringBuilder solution(String[] survey, int[] choices) {
    //     StringBuilder answer = new StringBuilder();
    //     int[] resultArr = new int[4];
    //     int[] scoreArr = {3, 2, 1, 0, -1, -2, -3};
        
    //     for (int i = 0; i < survey.length; i++) {
    //         switch (survey[i]) {
    //             case "RT": resultArr[0] += scoreArr[choices[i] - 1];
    //                 break;
    //             case "TR": resultArr[0] -= scoreArr[choices[i] - 1];
    //                 break;
    //             case "CF": resultArr[1] += scoreArr[choices[i] - 1];
    //                 break;
    //             case "FC": resultArr[1] -= scoreArr[choices[i] - 1];
    //                 break;
    //             case "JM": resultArr[2] += scoreArr[choices[i] - 1];
    //                 break;
    //             case "MJ": resultArr[2] -= scoreArr[choices[i] - 1];
    //                 break;
    //             case "AN": resultArr[3] += scoreArr[choices[i] - 1];
    //                 break;
    //             default: resultArr[3] -= scoreArr[choices[i] - 1];
    //         }
    //     }
        
    //     if (resultArr[0] >= 0) answer.append("R");
    //     else answer.append("T");
        
    //     if (resultArr[1] >= 0) answer.append("C");
    //     else answer.append("F");
        
    //     if (resultArr[2] >= 0) answer.append("J");
    //     else answer.append("M");
        
    //     if (resultArr[3] >= 0) answer.append("A");
    //     else answer.append("N");
        
    //     return answer;
    // }

    // 완주하지 못한 선수
    // public static void main(String[] args) {
    //     System.out.println(solution(new String[]{"leo", "kiki", "eden"}, new String[]{"eden", "kiki"}));
    // }

    // public String solution(String[] participant, String[] completion) {
    //     Map<String, Integer> entry = new HashMap<>();
        
    //     for (int i = 0; i < participant.length; i++) entry.put(participant[i], entry.getOrDefault(participant[i], 0) + 1);
        
    //     for (int i = 0; i < completion.length; i++) entry.put(completion[i], entry.get(completion[i]) - 1);
        
    //     for (Map.Entry<String, Integer> e : entry.entrySet()) {
    //         if (e.getValue() == 1) {
    //             return e.getKey();
    //         }
    //     }
        
    //     return null;
    // }
    //    푸드 파이트 대회
//    public static void main(String[] args) {
//        System.out.println(solution(new int[] {1, 3, 4, 6}));
//    }
//
//    public static String solution(int[] food) {
//        String answer = "0";
//
//        for (int i = food.length - 1; i > 0; i--) {
//            for (int j = 0; j < food[i] / 2; j++) {
//                answer = i + answer + i;
//            }
//        }
//
//        return answer;
//    }
//    기사단원의 무기
//    public static void main(String[] args) {
//        System.out.println(solution(10, 3, 2));
//    }
//
//    public static int solution(int number, int limit, int power) {
//        int answer = number;
//
//        for (int i = 2; i <= number; i++) {
//            // 약수 구하기
//            int yak = 0;
//            for (int j = 2; j <= i; j++) {
//                if (yak == limit - 1) {
//                    yak = power - 1;
//                    break;
//                } else if (i % j == 0) yak++;
//            }
//            answer += yak;
//        }
//
//        return answer;
//    }

    // 소수 만들기
//    public static void main(String[] args) {
//        System.out.println(solution(3, 4, new int[]{1,2,3,1,2,3,1}));
//    }
//
//    public static int solution(int k, int m, int[] score) {
//        int answer = 0;
//        List<Integer> temp = new ArrayList<>();
//
//        Arrays.sort(score);
//
//        for (int i = score.length - 1; i >= 0; i--) {
//            if (temp.size() > m) {
//                for (int j = 0; j < temp.size(); j++) {
//                    answer += temp.indexOf(j);
//                }
//                temp.clear();
//            }
//            if (temp > k) temp.add(k);
//            else temp.add(score[i]);
//        }
//
//        return answer;
//    }

    // 재귀
//    public static void main(String[] args) {
//        test1(10);
//    }
//
//    public static void test1(int cnt) {
//        if (cnt <= 0) {
//            System.out.println("발사");
//            return;
//        }
//        System.out.println(cnt + " 초");
//        test1(cnt - 1);
//    }

//    public static void main(String[] args) {
//        int[] arr = {2, 5, 10, 50, 51, 100, 105, 200};
//        int num = 105;
////        System.out.println(binarySearch(arr, num));
////        System.out.println(binarySearch2(arr, num, 0, arr.length - 1));
//
//        String testStr = "hong gil dong";
//        System.out.println(reverse(testStr));
//        System.out.println(reverse2(testStr));
//    }

    // while문으로 인덱스 찾기
//    public static int binarySearch(int[] arr, int num) {
//        int left = 0;
//        int right = arr.length - 1;
//
//        while (left <= right) {
//            int mid = left + (right - left) / 2;
//            if (arr[mid] == num) return mid;
//            else if (arr[mid] < num) left = mid + 1;
//            else right = mid - 1;
//        }
//        return -1;
//    }

    // 재귀로 인덱스 찾기
//    public static int binarySearch2(int[] arr, int num, int left, int right) {
//        if (left <= right) {
//            int mid = left + (right - left) / 2;
//
//            if (arr[mid] == num) return mid;
//
//            else if (arr[mid] < num) return binarySearch2(arr, num, mid + 1, right);
//            else return binarySearch2(arr, num, left, mid - 1);
//        }
//        return -1;
//    }

    // 문자열 뒤집기 for문
//    public static String reverse(String str) {
//        char[] chars = str.toCharArray();
//        for (int i = 0; i < chars.length / 2; i++) {
//            char temp = chars[i];
//            chars[i] = chars[chars.length - i - 1];
//            chars[chars.length - i - 1] = temp;
//        }
//        return new String(chars);
//    }

    // 문자열 뒤집기 재귀
//    public static String reverse2(String str) {
//        if (str.isEmpty()) return str;
//        return reverse(str.substring(1)) + str.charAt(0);
//    }

// 덧칠하기
    public static void main(String[] args) {
        System.out.println(solution(4, 1, new int[]{1, 2, 3, 4}));
    }

    public static int solution(int n, int m, int[] section) {
        int answer = 1;
        int temp = section[0];

        for (int i = 0; i < section.length; i++) {
            if (temp <= section[i] - m) {
                answer++;
                temp = section[i];
            }
        }

        return answer;
    }
}
